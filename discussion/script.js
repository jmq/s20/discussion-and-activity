console.log('Hello World')

//JSON Objects
	/*
		JSON - JavaScript Object Notation
		JS Object are not the same as JSON

		Syntax:
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}

	*/

//JSON Objects

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }


//JSON Array
// "cities": [
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Pasig City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{
// 		"city": "Makati City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	}
// ]

//JSON Method
//The JSON Object contains methods for parsing and converting data into a stringified JSON

let batchesArr = [
	{batch: "Batch 157"},
	{batch: "Batch 158"},
	{batch: "Batch 159"},
]
console.log(batchesArr)

//Stringify
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data)